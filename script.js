// TASK 1

const btn = document.getElementById('btn');
const text = document.getElementById('text');

function changeText() {
  setTimeout(() => {
    text.textContent = 'Operation has been successfully completed';
  }, 1000)
}
btn.addEventListener('click', () => {
  changeText()
});

//TASK 2


document.addEventListener('DOMContentLoaded', function () {
  const intervalCounter = document.getElementById('intervalCounter');
  intervalCounter.textContent = 10;
  let timer = setInterval(() => {
    if (+intervalCounter.textContent !== 0){
      +intervalCounter.textContent--;
    } else if (+intervalCounter.textContent === 0) {
      intervalCounter.textContent = 'Зворотній відлік завершено';
      clearInterval(timer);
    }
  }, 500);
})


